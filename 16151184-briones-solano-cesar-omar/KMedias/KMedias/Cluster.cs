﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EjercicioKMeans
{
    class Cluster
    {
        public String nombre;
        public List<Elemento> elementos = new List<Elemento>();
        public List<Elemento> elementosAnteriores = new List<Elemento>();

        public Cluster(string nombre, List<Elemento> elementos, List<Elemento> elementosAnteriores)
        {
            this.nombre = nombre;
            this.elementos = elementos;
            this.elementosAnteriores = elementosAnteriores;
        }

    }
}
