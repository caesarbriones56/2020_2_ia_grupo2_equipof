﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace KMedias
{
    class LectorDatos
    {
        string LineaActual;
        string PathFuente;
        StreamReader ArchivoFuente;
        StringBuilder Texto;

        public LectorDatos(string PathFuente = ""){
            if (PathFuente.Length>0) {
                LeerDatos(PathFuente);
            }
        }

        public int LeerDatos(string PathFuente){
            this.PathFuente = PathFuente;
            ArchivoFuente = new StreamReader(PathFuente);
            Texto = new StringBuilder();

            LineaActual = ArchivoFuente.ReadLine();

            int i=0; //contador

            while (LineaActual != null)
            {
                i++;
                Texto.Append(LineaActual);
                Texto.Append("\n");
                //Texto.Append(i);
                LineaActual = ArchivoFuente.ReadLine();
            }
            return i-1; //numero de registros
        }

        public string GetTexto() {
            return Texto.ToString();
        }

    }
}
