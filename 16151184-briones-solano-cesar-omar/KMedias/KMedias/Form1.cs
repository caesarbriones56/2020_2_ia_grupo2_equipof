﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KMedias
{
    public partial class Form1 : Form
    {
        LectorDatos MiLectorDatos;
        public Form1()
        {
            InitializeComponent();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void abrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OFDTexto.ShowDialog();
            if (OFDTexto.ShowDialog() == DialogResult.OK) {
                MiLectorDatos = new LectorDatos(OFDTexto.FileName);
                RTBTextoArchivo.Text = MiLectorDatos.GetTexto();
            }
        }
    }
}
