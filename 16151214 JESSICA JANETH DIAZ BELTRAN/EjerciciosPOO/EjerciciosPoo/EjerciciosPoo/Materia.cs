﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjerciciosPoo
{
    class Materia
    {
        string Clave;
        string Nombre;
        string Profesor;
        int Semestre;
        int Creditos;
        int Calificacion;
        int Intentos;

        public Materia(string Clave, string Nombre, int Creditos)
        {
            this.Clave = Clave;
            this.Nombre = Nombre;
            this.Creditos = Creditos;
            Calificacion = 0;
        }

        public bool Cursar(int Calificacion)
        {
            if (OportunidadesAgotadas())
            return false; 

            if (Calificacion < 70) {
                Calificacion = 0;
                Intentos++;
            }
            else
            {
                this.Calificacion = Calificacion;

            }
            return true;
        }

        public bool YaEstaAprobada()
        {
            return Calificacion != 0;
        }

        public bool YaSeCursoLaMateria()
        {
            return Intentos > 1 || Calificacion != 0;
        }

        public bool OportunidadesAgotadas()
        {
            return Intentos > 3;
        }
    }

   
}
