﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjerciciosPoo
{
    class Alumno
    {
        string NumControl;
        string Nombre;
        int NumSemestre;
        string Carrera;
        int Promedio;
        int CreditosAcumulados;

        List<Materia> Materias;

        public Alumno(string NumControl, string Nombre)
        {
            this.NumControl = NumControl;
            this.Nombre = Nombre;
            NumSemestre = 1;
            Promedio = 0;
            CreditosAcumulados = 0;
            Materias = new List<Materia>();
        }
        

    }
}
