﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemploGeneticos
{
    
    class Poblacion
    {
 
        List<Individuo> Individuos;
        List<Individuo> IndividuosSeleccionados;
        Random GeneradorAleatorios;
        EvaluadorTipo1 Evaluador;

        public Poblacion(int tam)
        {
            Individuos = new List<Individuo>();
            GeneradorAleatorios = new Random();
            Evaluador = new EvaluadorTipo1("12345678");


            for (int i = 0; i < tam; i++)
            
                Individuos.Add(new Individuo(Evaluador, GeneradorAleatorios));

            //OrdenarPorFitness();
        }

        public string GetGenotipoYFitness()
        {
            StringBuilder datos = new StringBuilder();

            for (int i = 0; i < Individuos.Count; i++)
                datos.Append(Individuos[i].GetGenotipo() + "" + Individuos[i].GetFitness() + "\n");                               
            return datos.ToString();
        }

        public string GetGenotipoYFitnessSeleccionados()
        {
            StringBuilder datos = new StringBuilder();
            for (int i = 0; i < IndividuosSeleccionados.Count; i++)
                datos.Append(IndividuosSeleccionados[i].GetGenotipo() + "" + IndividuosSeleccionados[i].GetFitness() + "\n");
            return datos.ToString();
        }

        public void SeleccionSimple(float presion)
        {
            IndividuosSeleccionados = new List<Individuo>();

            int cantidad_seleccionados = (int) Math.Round((100.0 - presion) * Individuos.Count / 100);

            for (int i=0; i < cantidad_seleccionados; i++)
                IndividuosSeleccionados.Add(Individuos[i]);
        }
        public void OrdenarPorFitness()
        {
            Individuo individuo_auxiliar = null;
            for (int i = 0; i < Individuos.Count-1; i++) 
                for (int j = i+1; j < Individuos.Count; j++)
                    if (Individuos[j].GetFitness() > Individuos[i].GetFitness())
                    {
                        individuo_auxiliar = Individuos[i];
                        Individuos[i] = Individuos[j];
                        Individuos[j] = individuo_auxiliar;
                    }
                        
        }
        public void Cruzamiento()
        {
            int catidad_hijos = Individuos.Count - IndividuosSeleccionados.Count;
            int indice_padre;
            int indice_madre;


            for (int i = 0; i < catidad_hijos; i++)
            {
                indice_padre = GeneradorAleatorios.Next(IndividuosSeleccionados.Count);
                indice_madre = GeneradorAleatorios.Next(IndividuosSeleccionados.Count);

                Individuos[IndividuosSeleccionados.Count + i] = new Individuo(Evaluador, GeneradorAleatorios, IndividuosSeleccionados[indice_padre], IndividuosSeleccionados[indice_madre]);

            }

            OrdenarPorFitness();
        }

        public void Mutacion(float porcentaje_mutacion)
        {
            for (int i = 0; i < Individuos.Count; i++)
                Individuos[i].Mutar(porcentaje_mutacion);
            
            //OrdenarFitness();
        }
    }
}
