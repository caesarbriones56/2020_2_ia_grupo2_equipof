﻿namespace EjemploGeneticos
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.RTBPoblacionActual = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.NUDTamPoblacion = new System.Windows.Forms.NumericUpDown();
            this.NUDPresion = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.RTBSeleccionados = new System.Windows.Forms.RichTextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.RTBCruzar = new System.Windows.Forms.RichTextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.RTBMutacion = new System.Windows.Forms.RichTextBox();
            this.NUDProbabilidaddeMutacion = new System.Windows.Forms.NumericUpDown();
            this.Mutacion = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.NUDTamPoblacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDPresion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDProbabilidaddeMutacion)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(131, 7);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Inicializar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // RTBPoblacionActual
            // 
            this.RTBPoblacionActual.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.RTBPoblacionActual.Location = new System.Drawing.Point(12, 41);
            this.RTBPoblacionActual.Name = "RTBPoblacionActual";
            this.RTBPoblacionActual.Size = new System.Drawing.Size(194, 336);
            this.RTBPoblacionActual.TabIndex = 1;
            this.RTBPoblacionActual.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Poblacion:";
            // 
            // NUDTamPoblacion
            // 
            this.NUDTamPoblacion.Location = new System.Drawing.Point(75, 10);
            this.NUDTamPoblacion.Name = "NUDTamPoblacion";
            this.NUDTamPoblacion.Size = new System.Drawing.Size(50, 20);
            this.NUDTamPoblacion.TabIndex = 3;
            this.NUDTamPoblacion.Value = new decimal(new int[] {
            55,
            0,
            0,
            0});
            // 
            // NUDPresion
            // 
            this.NUDPresion.Location = new System.Drawing.Point(280, 10);
            this.NUDPresion.Name = "NUDPresion";
            this.NUDPresion.Size = new System.Drawing.Size(56, 20);
            this.NUDPresion.TabIndex = 4;
            this.NUDPresion.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(232, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Presión";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // RTBSeleccionados
            // 
            this.RTBSeleccionados.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.RTBSeleccionados.Location = new System.Drawing.Point(235, 41);
            this.RTBSeleccionados.Name = "RTBSeleccionados";
            this.RTBSeleccionados.Size = new System.Drawing.Size(182, 341);
            this.RTBSeleccionados.TabIndex = 6;
            this.RTBSeleccionados.Text = "";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(342, 7);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 7;
            this.button2.Text = "Seleccionar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // RTBCruzar
            // 
            this.RTBCruzar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.RTBCruzar.Location = new System.Drawing.Point(435, 41);
            this.RTBCruzar.Name = "RTBCruzar";
            this.RTBCruzar.Size = new System.Drawing.Size(192, 341);
            this.RTBCruzar.TabIndex = 8;
            this.RTBCruzar.Text = "";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(435, 7);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 9;
            this.button3.Text = "Cruzar";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // RTBMutacion
            // 
            this.RTBMutacion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.RTBMutacion.Location = new System.Drawing.Point(648, 41);
            this.RTBMutacion.Name = "RTBMutacion";
            this.RTBMutacion.Size = new System.Drawing.Size(195, 341);
            this.RTBMutacion.TabIndex = 10;
            this.RTBMutacion.Text = "";
            // 
            // NUDProbabilidaddeMutacion
            // 
            this.NUDProbabilidaddeMutacion.DecimalPlaces = 2;
            this.NUDProbabilidaddeMutacion.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.NUDProbabilidaddeMutacion.Location = new System.Drawing.Point(755, 10);
            this.NUDProbabilidaddeMutacion.Name = "NUDProbabilidaddeMutacion";
            this.NUDProbabilidaddeMutacion.Size = new System.Drawing.Size(50, 20);
            this.NUDProbabilidaddeMutacion.TabIndex = 13;
            this.NUDProbabilidaddeMutacion.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.NUDProbabilidaddeMutacion.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // Mutacion
            // 
            this.Mutacion.AutoSize = true;
            this.Mutacion.Location = new System.Drawing.Point(645, 12);
            this.Mutacion.Name = "Mutacion";
            this.Mutacion.Size = new System.Drawing.Size(104, 13);
            this.Mutacion.TabIndex = 12;
            this.Mutacion.Text = "Probabilida de Mutar";
            this.Mutacion.Click += new System.EventHandler(this.label3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(811, 7);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 11;
            this.button4.Text = "Mutar";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(924, 389);
            this.Controls.Add(this.NUDProbabilidaddeMutacion);
            this.Controls.Add(this.Mutacion);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.RTBMutacion);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.RTBCruzar);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.RTBSeleccionados);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.NUDPresion);
            this.Controls.Add(this.NUDTamPoblacion);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RTBPoblacionActual);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.NUDTamPoblacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDPresion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDProbabilidaddeMutacion)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RichTextBox RTBPoblacionActual;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown NUDTamPoblacion;
        private System.Windows.Forms.NumericUpDown NUDPresion;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox RTBSeleccionados;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.RichTextBox RTBCruzar;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.RichTextBox RTBMutacion;
        private System.Windows.Forms.NumericUpDown NUDProbabilidaddeMutacion;
        private System.Windows.Forms.Label Mutacion;
        private System.Windows.Forms.Button button4;
    }
}

