﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemploGeneticos
{
    class Individuo
    {
        int Fitness;
        string Genotipo;

        Random GeneradorAleatorios;

        EvaluadorGeneral Evaluador;

        public Individuo(EvaluadorGeneral Evaluador, Random GeneradorAleatorios)
        {
            this.Evaluador = Evaluador;
            Genotipo = "";
            this.GeneradorAleatorios = GeneradorAleatorios;

            for (int i = 0; i < Evaluador.TamGenotipo; i++)
                Genotipo += GeneradorAleatorios.Next(Evaluador.LimInferiroFenotipo, Evaluador.LimSuperiorFenotipo);
        }

        public Individuo(EvaluadorGeneral Evaluador, Random GeneradorAleatorios, Individuo Padre, Individuo Madre)
        {
            this.Evaluador = Evaluador;
            this.GeneradorAleatorios = GeneradorAleatorios;

            int punto_cruzamiento = GeneradorAleatorios.Next(Evaluador.TamGenotipo);
            Genotipo = Padre.Genotipo.Substring(0, punto_cruzamiento);
            Genotipo = Madre.Genotipo.Substring(punto_cruzamiento);
        }

        public int GetFitness()
        {
            Fitness = Evaluador.Evaluar (this);
            return Fitness;
        }

        public string GetGenotipo()
        {
            return Genotipo;
        }

        public void Mutar(float porcentaje_mutacion)
        {
            float probabilidad_mutacion = (porcentaje_mutacion/100);

            string GenotipoMutado = "";
            for (int i = 0; i < Evaluador.TamGenotipo; i++)
            {
                if (GeneradorAleatorios.NextDouble() < probabilidad_mutacion)
                    GenotipoMutado += GeneradorAleatorios.Next(Evaluador.LimInferiroFenotipo, Evaluador.LimSuperiorFenotipo);
                else
                {
                    GenotipoMutado += Genotipo[i];
                }
                Genotipo = GenotipoMutado;
            }
        }
    }
}
