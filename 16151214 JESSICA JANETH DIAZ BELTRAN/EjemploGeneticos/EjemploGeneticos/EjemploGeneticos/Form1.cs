﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EjemploGeneticos
{
    public partial class Form1 : Form
    {
        Poblacion LaPoblacion;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LaPoblacion = new Poblacion((int)NUDTamPoblacion.Value);

            RTBPoblacionActual.Text = LaPoblacion.GetGenotipoYFitness();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            LaPoblacion.SeleccionSimple((int)NUDPresion.Value);

            RTBSeleccionados.Text = LaPoblacion.GetGenotipoYFitnessSeleccionados();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            LaPoblacion.Cruzamiento();

            RTBCruzar.Text = LaPoblacion.GetGenotipoYFitness();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            LaPoblacion.Mutacion((float)NUDProbabilidaddeMutacion.Value);

            RTBMutacion.Text = LaPoblacion.GetGenotipoYFitness();

        }
    }
}
