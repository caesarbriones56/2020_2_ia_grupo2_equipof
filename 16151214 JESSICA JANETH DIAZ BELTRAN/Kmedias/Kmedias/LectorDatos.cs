﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Kmedias
{
    class LectorDatos
    {
        string LineaActual;
        string PathFuente;
        StreamReader ArchivoFuente;
        StringBuilder Texto;
        public LectorDatos(string PathFuente = "")
        {
            if (PathFuente.Length > 0)
            {
                LeerDatos(PathFuente);
            }
        }

        public void LeerDatos(string PathFuente)
        {
            this.PathFuente = PathFuente;
            ArchivoFuente = new StreamReader(PathFuente);
            Texto = new StringBuilder();

            LineaActual = ArchivoFuente.ReadLine();
            while (LineaActual != null)
            {
                Texto.Append(LineaActual + "\n");
                LineaActual = ArchivoFuente.ReadLine();
            }
             
        }

        public string GetTexto()
        {
            return Texto.ToString();
        }


    }
}
