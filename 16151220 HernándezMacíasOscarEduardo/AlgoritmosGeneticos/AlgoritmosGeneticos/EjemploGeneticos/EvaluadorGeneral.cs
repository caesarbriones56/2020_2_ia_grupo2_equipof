﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemploGeneticos
{
    class EvaluadorGeneral
    {
        public int TamGenotipo;
        public int LimInferiroFenotipo;
        public int LimSuperiorFenotipo;

        public virtual int Evaluar(Individuo individuo)
        {
            return 0;
        }

    }
}
