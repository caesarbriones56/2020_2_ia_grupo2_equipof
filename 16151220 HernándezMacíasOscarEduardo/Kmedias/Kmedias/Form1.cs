﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kmedias
{
    public partial class Form1 : Form
    {
        LectorDatos MilectorDatos;

        public Form1()
        {
            InitializeComponent();
        }

        private void abrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(OFDTexto.ShowDialog() == DialogResult.OK)
            {
                MilectorDatos = new LectorDatos(OFDTexto.FileName);
            }

            RTB_TextoArchivo.Text = MilectorDatos.GetTexto();

        }
    }
}
